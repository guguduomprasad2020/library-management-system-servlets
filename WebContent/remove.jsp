<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>remove</title>
<link href="style.css" rel="stylesheet">

</head>
<body style="background-color:powderblue;">
	<h2 align="center">Remove student details</h2>
	<hr />

	<p align="right">
		<a href="index.jsp">logOut</a>
	</p>
	<hr />
	<span>${error}</span>
	<form action="remove" method="post">

		<table align="center">
		
			<tr>
				<td>Roll Number:</td>
				<td><input type="text" name="rollNumber"
					placeholder="enter rollNumber" required autocomplete="off" /></td>
			</tr>

			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit"
					value="submit" text_align="center" />&nbsp;&nbsp;&nbsp; <input
					type="reset" value="cancel" text_align="center" /></td>
			</tr>
		</table>
	</form>

</body>
</html>
