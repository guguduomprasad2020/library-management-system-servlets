package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IStudentDetails;
import dao.StudentDetailsImpl;
import model.StudentDetails;

@WebServlet("/list")
public class AddController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		//response.setContentType("text/html");
		int s_no = Integer.parseInt(request.getParameter("s_no"));
		//System.out.println(s_no);
		String name = request.getParameter("name");
		int rollNumber = Integer.parseInt(request.getParameter("rollNumber"));
		String membershipStatus = request.getParameter("membershipStatus");

		IStudentDetails sImpl = new StudentDetailsImpl();
		StudentDetails studentdetails = new StudentDetails();

		studentdetails.setS_no(s_no);
		studentdetails.setName(name);
		studentdetails.setRollNumber(rollNumber);
		studentdetails.setMembershipStatus(membershipStatus);

		int result = sImpl.addStudentDetails(studentdetails);
		if (result == 1) {
			//HttpSession session = request.getSession(false);
			request.setAttribute("msg", name);

			request.getRequestDispatcher("success.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "student details are not added successfully!!!");
			request.getRequestDispatcher("add-student.jsp").forward(request, response);
		}

	}
}
