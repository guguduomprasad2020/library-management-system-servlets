package util;

public class Query {
	public static String adminAuthentication = "select * from admin where adminName=? and password=?";
	public static String addStudentDetails = "insert into Studentdetails values(?,?,?,?)";
	public static String viewStudentDetails = "select * from Studentdetails";
	public static String editStudentDetails = "update Studentdetails set name=?,rollNumber=?,membershipStatus=? where s_no=?";
	public static String removeStudentDetails = "delete from Studentdetails where rollNumber=?";
}