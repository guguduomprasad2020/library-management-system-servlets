package model;

public class StudentDetails {
	private int s_no;
	private String name;
	private int rollNumber;
	private String membershipStatus;

	public StudentDetails() {
		// TODO Auto-generated constructor stub
	}

	public StudentDetails(int s_no, String name, int rollNumber, String membershipStatus) {
		super();
		this.s_no = s_no;
		this.name = name;
		this.rollNumber = rollNumber;
		this.membershipStatus = membershipStatus;
	}

	public int getS_no() {
		return s_no;
	}

	public void setS_no(int s_no) {
		this.s_no = s_no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getRollNumber() {
		return rollNumber;
	}

	public void setRollNumber(int rollNumber) {
		this.rollNumber = rollNumber;
	}

	public String getMembershipStatus() {
		return membershipStatus;
	}

	public void setMembershipStatus(String membershipStatus) {
		this.membershipStatus = membershipStatus;
	}

}