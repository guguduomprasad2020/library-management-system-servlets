package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IAdmin;
import dao.AdminImpl;
import model.Admin;


@WebServlet("/valid")
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out =response.getWriter();
		String name=request.getParameter("name");
		String password=request.getParameter("password");
		
		Admin valid=new Admin(name,password);
		IAdmin ivalid = new AdminImpl();
		int result =ivalid.authenticate(valid);
		if(result==0) {
			request.setAttribute("error", "username or password incorrect");
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}else {
			HttpSession session =request.getSession(true);
			session.setAttribute("uname", name);
			out.print("Hii "+name);
			request.getRequestDispatcher("details.jsp").forward(request, response);
			
		}
	}

}
