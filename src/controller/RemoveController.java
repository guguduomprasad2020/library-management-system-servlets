package controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IStudentDetails;
import dao.StudentDetailsImpl;
import model.StudentDetails;

@WebServlet("/remove")
public class RemoveController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int rollNumber = Integer.parseInt(request.getParameter("rollNumber"));
		IStudentDetails sImpl = new StudentDetailsImpl();
		StudentDetails sd = new StudentDetails();
		sd.setRollNumber(rollNumber);
		int result = sImpl.removeStudentDetails(sd);
		if (result == 1) {

			request.getRequestDispatcher("student-removed.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "Student details are not removed successfully!!!");
			request.getRequestDispatcher("remove.jsp").forward(request, response);
		}
	}

}
