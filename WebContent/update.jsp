<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>update</title>
<link href="style.css" rel="stylesheet">

</head>
<body><header>Updation</header>
	<h2 align="center">update Student details</h2>
	<hr />

	<p align="right">
		<a href="index.jsp">logOut</a>
	</p>
	<hr />
	<form action="update" method="post">
		<table align="center" cellspacing="30">
			<tr>
				<td>SNO:</td>
				<td><input type="text" name="s_no"
					 required autocomplete="off" /></td>
			</tr>
			<tr>
				<td>Name:</td>
				<td><input type="text" name="name" />
				</td>
			</tr>
			<tr>
				<td>Roll Number:</td>
				<td><input type="text" name="rollNumber"
					placeholder="enter roll number" required autocomplete="off" /></td>
			</tr>
			<tr>
			<td>MemberShip Status</td>
			<td><input type="text" name="membershipStatus"/></td>
			</tr>

			<tr>
				<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit"
					value="submit" text_align="center" />&nbsp;&nbsp;&nbsp; <input
					type="reset" value="cancel" text_align="center" /></td>
			</tr>
		</table>
	</form>

</body>
</html>
