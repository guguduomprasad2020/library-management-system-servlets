<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view Student details</title>
<link href="style.css" rel="stylesheet">

</head>
<body>
	<header align="center">View student details</header>

	<hr />
	<p align="right">
		<a href="index.jsp">logOut</a>
	</p>
	<table align="center" border=1>
		<tr>
			<th>S.no:</th>
			<th>Name:</th>
			<th>Roll Number:</th>
			<th>MembershipStatus:</th>

		</tr>
		<c:forEach items="${List}" var="s">
			<tr>
				<td>${s.getS_no()}</td>
				<td>${s.getName()}</td>
				<td>${s.getRollNumber()}</td>
				<td>${s.getMembershipStatus()}</td>

			</tr>

		</c:forEach>

	</table>


</body>
</html>
