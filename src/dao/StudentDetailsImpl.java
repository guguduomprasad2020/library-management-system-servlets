package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.StudentDetails;
import util.Db;
import util.Query;

public class StudentDetailsImpl implements IStudentDetails {
	PreparedStatement pst = null;
	ResultSet rs = null;
	int result = 0;

	@Override
	public int addStudentDetails(StudentDetails studentdetails) {
		try {
			pst = Db.getCon().prepareStatement(Query.addStudentDetails);
			pst.setInt(1, studentdetails.getS_no());
			pst.setString(2, studentdetails.getName());
			pst.setInt(3, studentdetails.getRollNumber());
			pst.setString(4, studentdetails.getMembershipStatus());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			result = 0;
		}
		finally {
			try {
				Db.getCon().close();
				pst.close();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public List<StudentDetails> viewStudentDetails() {
		List<StudentDetails> studentdetails = new ArrayList<StudentDetails>();
		try {
			pst = Db.getCon().prepareStatement(Query.viewStudentDetails);
			rs = pst.executeQuery();
			while (rs.next()) {
				StudentDetails e = new StudentDetails(rs.getShort(1), rs.getString(2), rs.getInt(3), rs.getString(4));
				studentdetails.add(e);
			}

		} catch (ClassNotFoundException | SQLException e) {
			try {
				Db.getCon().close();
				pst.close();
				rs.close();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

		}
		return studentdetails;
	}

	@Override
	public int editStudentDetails(StudentDetails studentdetails) {
		try {
			pst = Db.getCon().prepareStatement(Query.editStudentDetails);
			pst.setInt(4, studentdetails.getS_no());
			pst.setString(1, studentdetails.getName());
			pst.setInt(2, studentdetails.getRollNumber());
			pst.setString(3, studentdetails.getMembershipStatus());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			result = 0;
		} finally {
			try {
				Db.getCon().close();
				pst.close();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}

	@Override
	public int removeStudentDetails(StudentDetails studentdetails) {
		try {
			pst = Db.getCon().prepareStatement(Query.removeStudentDetails);
			pst.setInt(1, studentdetails.getRollNumber());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			result = 0;
		} finally {
			try {
				Db.getCon().close();
				pst.close();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
}
