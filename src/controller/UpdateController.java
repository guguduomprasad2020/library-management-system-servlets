package controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.StudentDetailsImpl;
import dao.IStudentDetails;
import model.StudentDetails;

@WebServlet("/update")
public class UpdateController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		int s_no = Integer.parseInt(request.getParameter("s_no"));
		String name = request.getParameter("name");
		int rollNumber = Integer.parseInt(request.getParameter("rollNumber"));
		String membershipStatus = request.getParameter("membershipStatus");

		IStudentDetails lopImpl = new StudentDetailsImpl();
		StudentDetails lop = new StudentDetails();
		lop.setS_no(s_no);
		lop.setName(name);
		lop.setRollNumber(rollNumber);
		lop.setMembershipStatus(membershipStatus);

		int result = lopImpl.editStudentDetails(lop);
		if (result == 1) {
			HttpSession session = request.getSession(false);
			request.setAttribute("msg", s_no);

			request.getRequestDispatcher("updated.jsp").forward(request, response);
		} else {
			request.setAttribute("error", "Student details are not updated successfully!!!");
			request.getRequestDispatcher("update.jsp").forward(request, response);
		}

	}

}
