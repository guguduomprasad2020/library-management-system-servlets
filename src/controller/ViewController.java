package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.IStudentDetails;
import dao.StudentDetailsImpl;
import model.StudentDetails;

@WebServlet("/appear")
public class ViewController extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		IStudentDetails lopImpl = new StudentDetailsImpl();
		List<StudentDetails> List= lopImpl.viewStudentDetails();
		request.setAttribute("List", List);
		request.getRequestDispatcher("view.jsp").forward(request, response);
	}

}
