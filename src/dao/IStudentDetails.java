package dao;

import java.util.List;

import model.StudentDetails;

public interface IStudentDetails {
	public int addStudentDetails(StudentDetails studentdetails);

	public List<StudentDetails> viewStudentDetails();

	public int editStudentDetails(StudentDetails studentdetails);

	public int removeStudentDetails(StudentDetails studentdetails);

}
